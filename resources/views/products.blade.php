<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Test site</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdn.rawgit.com/twbs/bootstrap/v4-dev/dist/css/bootstrap.css" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/css/tether.min.css">
</head>
<body>
<header class="navbar navbar-light navbar-fixed-top bd-navbar" role="banner">
    <div class="clearfix">
        <button class="navbar-toggler pull-xs-right hidden-sm-up" type="button" data-toggle="collapse" data-target="#bd-main-nav">
            &#9776;
        </button>
        <a class="navbar-brand hidden-sm-up" href="/">
            Home
        </a>
    </div>
</header>


<main id="content" role="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div>
                    {!! Form::open( ['route' => 'products.store', 'id' => 'productForm'] ) !!}

                    <div class="form-group">
                        {!! Form::label('name', 'Product name') !!}
                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('quantity', 'Quantity in stock') !!}
                        {!! Form::text('quantity', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('price', 'Price per item') !!}
                        {!! Form::text('price', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::submit( 'Add', ['class' => 'btn btn-primary', 'id' => 'btn-submit'] ) !!}
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table id="productsData" class="table table-stripped">
                    <tr>
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Price</th>
                    </tr>
                </table>
            </div>
        </div>
</main>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js"></script>
<script src="https://cdn.rawgit.com/twbs/bootstrap/v4-dev/dist/js/bootstrap.js"></script>

<script type="text/javascript">

    $("#productForm").on('submit', function(e) {
        e.preventDefault();

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/products',
            data: $(this).serialize(),
            success: function(data) {
                fillData(data);
            },
            failure: function (data) {
                alert('Please try again!');
            }
        });
    });

    function fillData(data) {
        for (var i = 0; i < data.length; i++) {
            fillRow(data[i]);
        }
    }

    function fillRow(rowData) {
        var row = $("<tr />")
        $("#productsData").append(row);
        row.append($("<td>" + rowData.name + "</td>"));
        row.append($("<td>" + rowData.quantity + "</td>"));
        row.append($("<td>" + rowData.price + "</td>"));
    }

</script>
</body>
</html>