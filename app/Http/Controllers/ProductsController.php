<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductsController extends Controller {

	/**
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		$products = [];
		return view('products', compact('products'));
	}

	/**
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		// set vars
		$data = [];
		$input = $request->all();
		$file = storage_path() . '/products.json';

		if (file_exists($file))
		{
			$data = json_decode(file_get_contents($file));
		}

		// remove csrf token
		unset($input['_token']);

		array_push($data, $input);

		file_put_contents($file, json_encode($data), FILE_APPEND);

		return response()->json($data);
	}
}